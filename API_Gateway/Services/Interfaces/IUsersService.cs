﻿using API_Data.Entities;
using API_Data.Models.Request;

namespace API_Gateway.Services.Interfaces
{
    public interface IUsersService
    {
        public Task<string> Register(UserRegisterRequest request);
        public Task<string> Login(UserLoginRequest request);
    }
}
