﻿using API_Gateway.Controllers.URLs;
using API_Gateway.Services.Interfaces;
using Newtonsoft.Json;
using System.Text;
using API_Data.Entities;
using System.Security.Cryptography;
using System.Security.Claims;
using API_Data.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using API_Data.Models.Request;

namespace API_Gateway.Services
{
    public class UsersService : IUsersService
    {
        private readonly HttpClient _httpClient;

        public UsersService(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("UsersApi");
        }

        public async Task<string> Register(UserRegisterRequest request)
        {
            var a = JsonConvert.SerializeObject(request);
            var data = new StringContent(a, Encoding.UTF8, "application/json");
            
            var b = await _httpClient.PostAsync(URLUsersMicroservice.Register, data);

            b.EnsureSuccessStatusCode();

            return await b.Content.ReadAsStringAsync();
        }

        public async Task<string> Login(UserLoginRequest request)
        {
            var a = JsonConvert.SerializeObject(request);
            var data = new StringContent(a, Encoding.UTF8, "application/json");

            var b = await _httpClient.PostAsync(URLUsersMicroservice.Login, data);

            b.EnsureSuccessStatusCode();

            return await b.Content.ReadAsStringAsync();
        }

    }
}
