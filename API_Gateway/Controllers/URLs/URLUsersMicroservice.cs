﻿namespace API_Gateway.Controllers.URLs
{
    public static class URLUsersMicroservice
    {
        public static string Register = @"Auth/register";
        public static string Login = @"Auth/login";
    }
}
