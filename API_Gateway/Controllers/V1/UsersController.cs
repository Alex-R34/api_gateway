﻿using API_Data.Entities;
using API_Data.Models.Request;
using API_Gateway.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_Gateway.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _users;

        public UsersController(IUsersService users)
        {
            _users = users;
        }

        [HttpPost("register")]
        public async Task<string> Register(UserRegisterRequest request)
        {
            return await _users.Register(request);
        }

        [HttpPost("login")]
        public async Task<string> Login(UserLoginRequest request)
        {
            return await _users.Login(request);
        }

        [HttpGet("usa")]
        [Authorize(Roles = "Usa")]
        public IActionResult Usa()
        {
            return Ok("USA");
        }

        [HttpGet("china")]
        [Authorize(Roles = "China")]
        public IActionResult China()
        {
            return Ok("USA");
        }
    }
}
