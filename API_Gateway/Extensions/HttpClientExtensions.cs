﻿using API_Gateway.Services;
using API_Gateway.Services.Interfaces;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;
using System.Net;

namespace API_Gateway.Extensions
{
    public static class HttpClientExtensions
    {
        public static void AddUsersHttpClientWithCircuitbreaker(this IServiceCollection services)
        {
            services.AddHttpClient<IUsersService, UsersService>("UsersApi", client =>
            {
                client.BaseAddress = new Uri("https://localhost:7208/api/");
            })
                .AddPolicyHandler(HttpPolicyExtensions.HandleTransientHttpError()
                .Or<Exception>()
                .Or<TimeoutRejectedException>()
                .OrResult(x => x.StatusCode == (HttpStatusCode)209)
                .AdvancedCircuitBreakerAsync(
                    0.6, // от 0 до 1 процент отказов при котором цепь разорвётся
                    TimeSpan.FromSeconds(15), // в каком промежутке времени будет считаться колличество ошибок
                    2,
                    TimeSpan.FromSeconds(15) // длительность отключения
                    ));
            //.AddTransientHttpErrorPolicy(c => c.CircuitBreakerAsync(2, TimeSpan.FromSeconds(30)));
        }
    }
}
