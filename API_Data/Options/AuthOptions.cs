﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Data.Options
{
    public class AuthOptions
    {
        public const string ISSUER = "https://localhost:7020"; // издатель токена
        public const string AUDIENCE = "https://localhost:7020"; // потребитель токена
        const string KEY = "583e4ec5-6ead-4a23-952b-539b6b787935";   // ключ для шифрации
        public static SymmetricSecurityKey GetSymmetricSecurityKey() =>
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY));
    }
}
