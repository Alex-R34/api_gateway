﻿using API_Data.Contexts;
using API_Data.Entities;
using API_Data.Models.Request;
using API_Data.Models.Responce;
using API_Data.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace UsersMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UsersContext _context;
        private readonly IConfiguration _config;

        public AuthController(UsersContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserRegisterResponce>> Register(UserRegisterRequest request)
        {
            CreatePaswordHash(request.Password, out byte[] passwordHash, out byte[] passwodSalt);

            User user = new User();
            user.Username = request.Username;
            user.Role = request.Role;
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwodSalt;

            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return new UserRegisterResponce { Role = request.Role, Username = request.Username}; // ДОБАВИТЬ МАППИНГ
        }

        [HttpPost("login")]
        public async Task<ActionResult<string>> Login(UserLoginRequest request)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == request.Username);

            if (user is null)
                return BadRequest();
            
            if (!VerifyPasswordHash(request.Password, user.PasswordHash, user.PasswordSalt))
                return BadRequest();

            var token = CreateToken(user);

            return Ok(token);
        }

        [HttpGet("usa")]
        [Authorize(Roles = "Usa")]
        public IActionResult GetUSA()
        {
            return Ok("USA");
        }

        [HttpGet("china")]
        [Authorize(Roles = "China")]
        public IActionResult GetChina()
        {
            return Ok("China");
        }

        private void CreatePaswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                //new Claim("Name", user.Username),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var key = AuthOptions.GetSymmetricSecurityKey();

            var creads = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(audience: AuthOptions.AUDIENCE,
                                             issuer: AuthOptions.ISSUER,
                                             claims: claims,
                                             expires: DateTime.Now.AddMinutes(5),
                                             signingCredentials: creads);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

    }
}
